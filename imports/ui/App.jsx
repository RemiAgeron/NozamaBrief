import React from 'react';
import { Meteor } from 'meteor/meteor'
import { Hello } from './Hello.jsx';
import { useTracker } from 'meteor/react-meteor-data';
import NavBar from './components/navbar/NavBar.jsx';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import About from './pages/About';
import { aboutPage, basketPage, inventoryPage, productPage, signUpPage } from '../constants'
import Disclaimer from './pages/Disclaimer.jsx';
import Inventory from './pages/Inventory.jsx';
import Product from './pages/Product.jsx';
import SignUpForm from './components/authentification/SignUpForm.jsx';
import Basket from './pages/Basket.jsx';

export const UserContext = React.createContext({ user: null, userId: null, isLoggedIn: false, status: 'public' });

export const App = () => {

  const useAccount = () => useTracker(() => {
    const user = Meteor.user();
    const userId = Meteor.userId();
    const status = user?.profile.status || 'public';
    return { user, userId, isLoggedIn: !!userId, status };
  }, []);

  const { user, userId, isLoggedIn, status } = useAccount();
  const providerUser = React.useMemo(() => ({ user, userId, isLoggedIn, status }), [ user, userId, isLoggedIn, status ])

  return (
    <div className='min-h-screen dark:bg-slate-600 dark:text-white'>
      <Router>
        <UserContext.Provider value={providerUser} >
        <NavBar />
          <Routes>
            <Route path={productPage.path} element={<Product />} />
            <Route path={aboutPage.path} element={<About />} />
            <Route path={basketPage.path} element={status != 'public' ? <Basket /> : <Disclaimer title={'erreur 403'} message={"Vous n'avez pas accès à cette page"} />} />
            <Route path={inventoryPage.path} element={status === 'admin' ? <Inventory /> : <Disclaimer title={'erreur 403'} message={"Vous n'avez pas accès à cette page"} />} />
            <Route path={signUpPage.path} element={status === 'admin' ? <SignUpForm /> : <Disclaimer title={'erreur 403'} message={"Vous n'avez pas accès à cette page"} />} />
          </Routes>
        </UserContext.Provider>
      </Router>
    </div>
  );
};
