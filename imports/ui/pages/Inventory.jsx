import React from 'react';
import { Products } from '../../api/collections';
import { columnsInventory } from '../../constants';
import ProductEditModal from '../components/modals/ProductEditModal';
import Pagination from '../components/Pagination';
import Button from '../components/Button';
import NewProductModal from '../components/modals/NewProductModal';
import { HeadColumns, BodyRows } from '../components/InventoryComponents';

// const data = [
//   {
//     title: "Apple MacBook Pro 17",
//     tags: ["Sliver",
//       "Laptop"],
//     price: 2999,
//     stock: 320
//   },

//   {
//     title: "Microsoft Surface Pro",
//     tags: ["White",
//       "Laptop PC"],
//     price: 1999,
//     stock: 58
//   },

//   {
//     title: "Magic Mouse 2",
//     tags: ["Black",
//       "Accessories"],
//     price: 99,
//     stock: 2
//   },

//   {
//     title: "Google Pixel Phone",
//     tags: ["Gray",
//       "Phone"],
//     price: 799,
//     stock: 973
//   },

//   {
//     title: "Apple Watch 5",
//     tags: ["Red",
//       "Wearables"],
//     price: 999,
//     stock: 0
//   },
// ]

const Inventory = () => {

  const [products, setProducts] = React.useState([]);
  // const [tags, setTags] = React.useState([]);

  const [editModal, setEditModal] = React.useState(false);
  const [propsModal, setPropsModal] = React.useState({});

  const [newProductModal, setNewProductModal] = React.useState(false);

  React.useEffect(() => {
    setProducts(Products.find());
    // setTags(Tags.find());
  }, [])

  return (
    <>
      {editModal ? <ProductEditModal product={propsModal} exitModal={() => setEditModal(false)} /> : null}
      {newProductModal ? <NewProductModal exitModal={() => setNewProductModal(false)} /> : null}
      <div className='space-y-8 col-flex w-[90%] mx-auto my-10'>
        <div className='mx-auto w-fit'>
          <Button message={'Ajouter un nouveau produit'} func={() => setNewProductModal(true)} />
        </div>
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr className='first:w-4/12 nth-child-3:text-center nth-child-4:text-center'>
                {columnsInventory.map(col => <HeadColumns key={col.title} title={col.title} hidden={col.hidden} />)}
              </tr>
            </thead>
            <tbody>
              {products.map(row => <BodyRows key={row._id} product={row} setPropsModal={setPropsModal} editModal={() => setEditModal(true)} />)}
            </tbody>
          </table>
        </div>
        {products > 10 ?
          <div className='w-fit mx-auto'>
            <Pagination />
          </div>
          :
          null
        }
      </div>
    </>
  );
};

export default Inventory;