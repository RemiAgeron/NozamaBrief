import React from 'react'

export const About = () => (
  <p className='w-[85%] mx-auto my-20 p-8 rounded-xl text-xl dark:text-gray-200 text-justify indent-8 leading-loose bg-gray-300 dark:bg-gray-800'>Nozama en France : une contribution positive à l’économie Nous servons avec passion nos clients en France. Toutefois, si nous pensons d’abord à eux, nous accordons également la plus grande importance à notre empreinte économique, sociale et environnementale au niveau local. En mettant toute notre énergie, notre savoir-faire et notre capacité d’innovation au service des Français, nous contribuons à la croissance de l’économie française. Nous souhaitions partager un aperçu de la manière dont nous contribuons à la croissance de l’économie française, à la création de milliers d’emplois, au financement des services publics et du modèle social français; tout en prenant soin de préserver l’environnement.</p>
)

export default About