import React from 'react';
import ProductGrid from '../components/productgrid/ProductGrid';

export const Product = () => {

    return (
        <div className='py-14'>
            <ProductGrid />
        </div>
    )
}

export default Product