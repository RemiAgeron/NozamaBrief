import React from 'react';
import { Meteor } from 'meteor/meteor'
import { useLocation, Link } from 'react-router-dom';
import LoginModal from '../modals/LoginModal';
import { allPages } from '../../../constants/index';
import { UserContext } from '../../App';
import Button from '../Button';

const NavBar = () => {

  const [displayMenu, setDisplayMenu] = React.useState(false);
  const toggleMenu = () => setDisplayMenu(!displayMenu);

  const [displayModal, setDisplayModal] = React.useState(false);

  const logout = () => Meteor.logout();

  const location = useLocation();

  const { isLoggedIn, status } = React.useContext(UserContext);

  const allowedPages = () => {
    switch (status) {
      case 'admin': return allPages;
      case 'customer': return allPages.filter(page => page.status != 'admin');
      default: return allPages.filter(page => page.status === "public");
    }
  }

  const Navlinks = () => (
    <div className={`${displayMenu ? null : 'hidden'} justify-between items-center w-full md:flex md:w-auto md:order-1`}>
      <ul className="flex flex-col mt-4 md:flex-row lg:space-x-8 space-x-4 md:mt-0 md:text-sm md:font-medium">
        {allowedPages().map(element => (
          <li key={element.title}>
            <Link className={`${element.path === location.pathname ? 'text-gray-700 dark:text-white ' : 'text-gray-700 dark:text-gray-400'} text-lg py-2 pr-4 pl-3 md:hover:text-blue-700 md:p-0 md:dark:hover:text-white dark:hover:text-white  `} to={element.path}>{element.title}</Link>
          </li>
        ))}
      </ul>
    </div>
  )

  return (
    <>
      {displayModal ?
        <LoginModal exitModal={() => setDisplayModal(false)} />
        :
        null
      }
      <div className='h-[60px]'>
        <nav className="fixed w-full top-0 bg-white border-gray-200 px-2 sm:px-4 py-2.5 dark:bg-gray-800 z-50">
          <div className="flex flex-wrap justify-between items-center mx-auto">
            <div className='flex'>
              <Link to={'/'} className="flex items-center lg:mr-14 mr-6">
                <img src='assets/70c29c6e-e708-409a-a54f-3162c528b92f.png' className="mr-3 h-6 sm:h-9" alt="Logo Nozama" />
                <span className="fontBangers self-center text-2xl dark:text-white">Nozama</span>
              </Link>
              {displayMenu ? null : <Navlinks />}
            </div>
            <div className="flex md:order-2">
              {isLoggedIn ?
                <button type="button" className="text-white bg-gray-700 hover:bg-gray-800 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-3 md:mr-0 dark:bg-gray-600 dark:hover:bg-gray-700 dark:focus:ring-gray-800" onClick={logout}>Déconnexion</button>
                :
                <Button message={'Connexion'} func={() => setDisplayModal(true)} />
              }
              <button type="button" className="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" onClick={toggleMenu}>
                <svg className="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd"></path></svg>
                <svg className="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd"></path></svg>
              </button>
            </div>
            {displayMenu ? <Navlinks /> : null}
          </div>
        </nav>
      </div>
    </>
  );
};

export default NavBar;