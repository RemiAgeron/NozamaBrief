import React from 'react';

const Star = ({ yellow }) => (
    <svg className={`w-5 h-5 ${yellow ? 'text-yellow-300' : 'text-gray-500'}`} fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path></svg>
);

export const ProductCard = ({ product }) => {

    const raters = Math.floor((Math.random() * 999) + 1);
    const rating = Math.floor((Math.random() * 500) + 1) / 100;
    const maxStars = 5;
    const yellowstars = Math.floor(rating);
    const ratingStars = [...Array(yellowstars).fill(<Star yellow />), ...Array(maxStars - yellowstars).fill(<Star />)];

    const priceTrunc = Math.trunc(product.price);
    const priceRest = Math.round((product.price - priceTrunc) * 100);
    const priceDecimal = priceRest < 10 ? '0' + priceRest : priceRest;

    const available = product.stock > 0;

    return (
        <div className="w-[300px] h-[438px] bg-white rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700 shadow grid content-between">
            <div>
                <div className='mx-auto p-8 w-[300px] h-[300px] grid content-center'>
                    <img src={product.image_link} alt={product.title} />
                </div>
                <h5 className="px-5 text-xl w-[300px] font-semibold truncate text-gray-900 dark:text-white">{product.title}</h5>
            </div>
            <div className="px-5 pb-5">
                <div className="flex items-center mt-2.5 mb-5">
                    {ratingStars.map((star) => star)}
                    <span className="bg-blue-100 text-blue-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800 ml-3">{rating}</span>
                    <span className='font-bold text-white text-sm align-middle'>({raters})</span>
                </div>
                <div className="flex justify-between items-center">
                    <span className='font-bold text-gray-900 dark:text-white'>
                        <span className="text-3xl">{priceTrunc}</span>
                        <span className='text-xl align-top'>€{priceDecimal}</span>
                    </span>
                    <button disabled={!available} className="disabled:cursor-not-allowed disabled:bg-gray-700 disabled:dark:bg-gray-500 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Ajouter au Panier</button>
                </div>
            </div>
        </div>
    );
};

export default ProductCard;