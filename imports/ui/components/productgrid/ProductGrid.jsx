import React from 'react';
import ProductCard from './ProductCard';
import { Products } from '../../../api/collections';

// const data = [
//     { title: 'PC Portable - HP  Pavillon 15- 15,6" FHD', price: 499, img: 'https://www.cdiscount.com/pdt2/2/n/f/1/300x300/hp15eh1022nf/rw/pc-portable-hp-pavillon-15-eh1022nf-15-6-fhd.jpg', rating: 3.4, raters: 43, available: false },

//     { title: 'Console Nintendo Switch (modèle OLED)', price: 339.95, img: 'https://www.cdiscount.com/pdt2/4/3/5/1/300x300/45496453435/rw/console-nintendo-switch-modele-oled-nouvelle-v.jpg', rating: 5.0, raters: 221, available: true },
//     { title: 'Machine expresso automatique avec broyeur - DELONGHI', price: 319.99, img: 'https://www.cdiscount.com/pdt2/3/2/6/1/300x300/del8004399325326/rw/machine-expresso-automatique-avec-broyeur-delong.jpg', rating: 5.0, raters: 1263, available: true },
//     { title: 'Salon de jardin en imitation rotin tressé Allibert - 4 personnes', price: 149.50, img: 'https://www.cdiscount.com/pdt2/t/g/r/1/300x300/sanremo4seatgr/rw/salon-de-jardin-en-imitation-rotin-tresse-allibert.jpg', rating: 5.0, raters: 131, available: true }
// ]

export const ProductGrid = () => {

  const [products, setProducts] = React.useState([]);
  // const [tags, setTags] = React.useState([]);

  const [editModal, setEditModal] = React.useState(false);
  const [propsModal, setPropsModal] = React.useState({});

  const [newProductModal, setNewProductModal] = React.useState(false);

  React.useEffect(() => {
    setProducts(Products.find());
    // setTags(Tags.find());
  }, [])

  return (
    <div className='flex flex-wrap gap-6 justify-center'>
      {products.map(element => (
        <ProductCard key={element._id} product={element} />
      ))}
    </div>
  )
}

export default ProductGrid