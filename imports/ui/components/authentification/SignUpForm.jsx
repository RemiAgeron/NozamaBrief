import React from 'react';
import { useForm } from "react-hook-form";
import { UserContext } from '../../App';
import { Accounts } from 'meteor/accounts-base';

export const SignUpForm = ({ closeModal, goLogIn }) => {

    const { status } = React.useContext(UserContext);

    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const onSubmit = formData => {
        if (formData.password === formData.confirmPassword) delete formData.confirmPassword;
        else throw new Error('Erreur Veuillez réessayez');
        formData.profile = { status: formData.profile ? 'admin' : 'customer' };
        Accounts.createUser(formData);
    }

    const errorRequiredMessage = <span className='text-red-500'>Ce champ est requis</span>;
    const errorMinLengthMessage = <span className='text-red-500'>8 charactères minimum</span>;
    const errorMatchPasswordMessage = <span className='text-red-500'>Les deux mots de passes doivent être identiques</span>;

    return (
        <div className="px-6 py-6 lg:px-8">
            <h3 className="mb-4 text-2xl font-medium text-gray-900 dark:text-white">Inscription d'un nouveau compte</h3>
            <form className="space-y-6" onSubmit={handleSubmit(onSubmit)}>
                <div className='space-y-1'>
                    <label className="block text-sm font-medium text-gray-900 dark:text-gray-300">Entrez votre prénom</label>
                    <input {...register("firstname", { required: true })} placeholder="Prénom" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
                    {errors.firstname && errorRequiredMessage}
                </div>
                <div className='space-y-1'>
                    <label className="block text-sm font-medium text-gray-900 dark:text-gray-300">Entrez votre nom</label>
                    <input {...register("lastname", { required: true })} placeholder="Nom" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
                    {errors.lastname && errorRequiredMessage}
                </div>
                <div className='space-y-1'>
                    <label className="block text-sm font-medium text-gray-900 dark:text-gray-300">Votre adresse mail</label>
                    <input {...register("email", { required: true })} type='email' className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="Adresse mail" />
                    {errors.email && errorRequiredMessage}
                </div>
                <div className='space-y-1'>
                    <label className="block text-sm font-medium text-gray-900 dark:text-gray-300">Votre adresse</label>
                    <input {...register("address")} type='text' placeholder="Adresse" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
                </div>
                <div>
                    <label className="block text-sm font-medium text-gray-900 dark:text-gray-300">Votre mot de passe</label>
                    <input type='password' {...register("password", { required: true, minLength: 8 })} placeholder="Mot de passe" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
                    {errors.password && errors.password.type === 'required' && errorRequiredMessage}
                    {errors.password && errors.password.type === 'minLength' && errorMinLengthMessage}
                </div>
                <div className='space-y-1'>
                    <label className="block text-sm font-medium text-gray-900 dark:text-gray-300">Entrez à nouveau le mot de passe</label>
                    <input type='password' {...register("confirmPassword", { required: true, validate: value => value === watch("password", "") || false })} placeholder="Confirmer mot de passe" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
                    {errors.confirmPassword && errors.confirmPassword.type === 'required' && errorRequiredMessage}
                    {errors.confirmPassword && errors.confirmPassword.type === 'validate' && errorMatchPasswordMessage}
                </div>
                {status === 'admin' ?
                    <label htmlFor="default-toggle" className="relative inline-flex items-center cursor-pointer">
                        <input type="checkbox" {...register("profile")} id="default-toggle" className="sr-only peer" />
                        <div className="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
                        <span className="ml-3 text-sm font-medium text-gray-900 dark:text-gray-300">Compte Administrateur</span>
                    </label>
                    :
                    null}
                <button type="submit" className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Valider</button>
                <div className="flex flex-wrap gap-4 text-sm font-medium text-gray-500 dark:text-gray-300">
                    <h3 className='my-auto'>Vous avez déjà un compte ?</h3>
                    <button className="text-lg text-blue-700 dark:text-blue-500" onClick={goLogIn}>Se Connecter</button>
                </div>
            </form>
        </div>
    );
};

export default SignUpForm;