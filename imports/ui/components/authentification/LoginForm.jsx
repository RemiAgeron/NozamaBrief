import { Meteor } from 'meteor/meteor';
import React from 'react';
import { useForm } from "react-hook-form";

export const LoginForm = ({ closeModal, goSignUp }) => {

  const { register, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = data => {
    Meteor.loginWithPassword(data.email, data.password);
    closeModal();
  };

  const errorMessage = <span className='text-red-500'>Ce champ est requis</span>;

  return (
    <div className="px-6 py-6 lg:px-8">
      <h3 className="mb-4 text-2xl font-medium text-gray-900 dark:text-white">Connexion à votre compte</h3>
      <form className="space-y-6" onSubmit={handleSubmit(onSubmit)}>
        <div className='space-y-1'>
          <label className="block text-sm font-medium text-gray-900 dark:text-gray-300">Entrez votre adresse mail</label>
          <input {...register("email", { required: true })} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="adresse mail" />
          {errors.lastname && errorMessage}
        </div>
        <div className='space-y-1'>
          <label className="block text-sm font-medium text-gray-900 dark:text-gray-300">Entrez un mot de passe</label>
          <input type='password' {...register("password", { required: true })} name="password" placeholder="mot de passe" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
          {errors.password && errorMessage}
        </div>
        <button type="submit" className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Se Connecter</button>
        <div className="flex flex-wrap gap-4 text-sm font-medium text-gray-500 dark:text-gray-300">
          <h3 className='my-auto'>Vous n'avez pas de compte ?</h3>
          <button className="text-lg text-blue-700 dark:text-blue-500" onClick={goSignUp}>S'inscrire</button>
        </div>
      </form>
    </div>
  );
};

export default LoginForm;