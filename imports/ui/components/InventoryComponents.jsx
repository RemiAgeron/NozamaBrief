import React from "react";

export const HeadColumns = ({ title, hidden }) =>
  <th scope="col" className="px-6 py-3">{hidden ? <span className="sr-only">{title}</span> : title}</th>;

export const BodyColumns = ({ title, width, center }) =>
  <th scope="row" className={`${width} ${center ? 'text-center' : null} truncate px-6 py-4 font-medium text-gray-900 dark:text-gray-200`}>{title}</th>;

export const BodyRows = ({ product, setPropsModal, editModal }) =>
  <tr className="border-b dark:bg-gray-800 dark:border-gray-700 odd:bg-white even:bg-gray-50 odd:dark:bg-gray-800 even:dark:bg-gray-700">
    <BodyColumns title={product.title} width='w-4/12' />
    <BodyColumns title={product.tags?.join(', ')} width='w-2/12' />
    <BodyColumns title={product.price + '€'} center width='w-1/12' />
    <BodyColumns title={product.stock} center width='w-1/12' />
    <td className="px-6 py-4 text-center w-1/12">
      <button onClick={() => { setPropsModal(product); editModal() }} className="font-medium text-blue-600 dark:text-blue-500">Modifier</button>
    </td>
  </tr>