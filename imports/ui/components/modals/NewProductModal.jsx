import React from 'react';
import { useForm } from "react-hook-form";
import { Products } from '../../../api/collections';
import Button from '../Button';

const NewProductModal = ({ exitModal }) => {

  const { register, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = formData => {
    if (Products.insert(formData)) exitModal();
  };


  return (
    <div className='absolute top-0 w-full h-screen z-50 bg-gray-500/50 overflow-y-auto'>
      <div className="relative mt-10 mb-5 w-[85%] min-h-fit mx-auto bg-white rounded-lg shadow dark:bg-slate-700 px-4 shadow">
        <button type="button" className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white" onClick={exitModal}>
          <svg className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd"></path></svg>
        </button>
        <div className="px-4 py-6">
          <h3 className="mb-4 text-2xl font-medium text-gray-900 dark:text-white">Ajout d'un nouveau produit</h3>
          <form className="space-y-10 pt-6" onSubmit={handleSubmit(onSubmit)}>
            <div className='space-y-6 px-10'>

              <div>
                <label className="block mb-2 font-medium text-gray-900 dark:text-gray-300">Le nom du produit</label>
                <input {...register("title", { required: true })} placeholder="nom du produit" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
                {errors.title && <span>Ce champ est requis</span>}
              </div>

              <div>
                <label className="block mb-2 font-medium text-gray-900 dark:text-gray-300">La description du produit</label>
                <textarea {...register("description")} placeholder="description du produit" rows="2" className="resize-none bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white overflow-y-auto"></textarea>
              </div>

              <div className='space-y-2'>
                <label className="block font-medium text-gray-900 dark:text-gray-300">Les étiquettes du produit</label>
                <div className='flex space-x'>
                </div>
                <select id="countries" placeholder='oui' class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                  <option selected>Selectionner une étiquette</option>
                </select>
              </div>

              <div>
                <label className="block mb-2 font-medium text-gray-900 dark:text-gray-300">Le lien de l'image du produit</label>
                <input {...register("image_link")} placeholder="https://..." className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
              </div>

              <div>
                <label className="block mb-2 font-medium text-gray-900 dark:text-gray-300">Le prix du produit</label>
                <input {...register("price", { required: true, pattern: /^\d{1,}(\.\d{1,2})?$/u })} placeholder="prix du produit" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
                {errors.price && errors.price.type === 'required' && <span>Ce champ est requis</span>}
                {errors.price && errors.price.type === 'pattern' && <span>Ne doit contenir des chiffres séparés par un point</span>}
              </div>

              <div>
                <label className="block mb-2 font-medium text-gray-900 dark:text-gray-300">La quantité en stock</label>
                <input {...register("stock", { required: true, pattern: /^\d*$/u })} placeholder='quantité en stock' className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
                {errors.stock && errors.stock.type === 'required' && <span>Ce champ est requis</span>}
                {errors.stock && errors.stock.type === 'pattern' && <span>Ne doit contenir des chiffres</span>}
              </div>

            </div>
            <div className='flex w-8/12 mx-auto space-x-14'>
              <Button submit message={'Valider'} />
              <Button option='cancel' message={'Annuler'} func={exitModal} />
            </div>
          </form>

        </div>
      </div>
    </div>
  )
};

export default NewProductModal;