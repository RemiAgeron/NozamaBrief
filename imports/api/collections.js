import { Mongo } from 'meteor/mongo';

// {message: string, stars: number, user_id: string, product_id: string}
export const Reviews = new Mongo.Collection('reviews');

// {title: string, description: string, image_link: string, price: number, stock: number }
export const Products = new Mongo.Collection('products');

// {product_id: string, tag_id: string}
export const ProductsTagsRelations = new Mongo.Collection('products-tags-relations');

// {title: string}
export const Tags = new Mongo.Collection('tags');

// {user_id: string, product_id: string, amount: number }
export const Baskets = new Mongo.Collection('baskets');