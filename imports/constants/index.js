// Status
export const pageStatus = {
  public: 'public',
  customer: 'customer',
  admin: 'admin'
};

// Pages
export const productPage = {title: 'Produits', path: '/', status: pageStatus.public};
export const basketPage = {title: 'Panier', path: '/panier', status: pageStatus.customer};
export const aboutPage = {title: 'À propos', path: '/a-propos', status: pageStatus.public};
export const inventoryPage = {title: 'Inventaire', path: '/inventaire', status: pageStatus.admin};
export const signUpPage = {title: 'Inscription', path: '/inscription', status: pageStatus.admin};

export const allPages = [
  productPage,
  basketPage,
  aboutPage,
  inventoryPage,
  signUpPage,
]

// Tables
export const columnsInventory = [
  {title:'Nom du produit'},
  {title:'Étiquettes'},
  {title:'Prix'},
  {title:'Stock'},
  {title:'Modifier', hidden: true}
]
