function nthChild(index){
  return function ({addVariant }) {
    addVariant(`nth-child-${index}`, ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          const css = `.nth-child-${index}\\${separator}${className}>*:nth-child(${index})`
          return css;
        });
    })
  }
}

module.exports = {
  content: ["./imports/ui/**/*.{js,jsx}"],
  theme: {
    extend: {},
  },
  plugins: [
    nthChild(2),
    nthChild(3),
    nthChild(4),
    nthChild(5),
  ],
}