import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Products } from '../imports/api/collections';

const SEED_ADMIN = { firstname: 'Benoît', lastname: 'Martin', email: 'benoit.martin@nozama.com', password: 'password', profile: { status: 'admin' } }
const SEED_CUSTOMER = { firstname: 'David', lastname: 'Iksyxix', email: 'david.iksyxix@local.fr', password: 'password', profile: { status: 'customer' } }

const data = [
  { title: "Apple MacBook Pro 17", description: "", image_link: "https://cdn.lesnumeriques.com/optim/produits/71/5859/71_5859_2__450_400.jpg", price: 2999, stock: 320 },
  { title: "Microsoft Surface Pro", description: "", image_link: "https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RWFM7r?ver=04a4&q=90&m=6&h=722&w=1083&b=%23FFFFFFFF&l=f&o=t&aim=true", price: 1999, stock: 58 },
  { title: "Magic Mouse 2", description: "", image_link: "https://novodesk.net/wp-content/uploads/2019/07/Magic-mouse-2-argent-1.jpg", price: 99, stock: 2 },
  { title: "Google Pixel Phone", description: "", image_link: "https://m.media-amazon.com/images/I/81-fNmQqlLL._AC_SY450_.jpg", price: 799, stock: 973 },
  { title: "Apple Watch 5", description: "", image_link: "https://www.apple.com/newsroom/images/product/watch/standard/Apple_watch_series_5-gold-aluminum-case-pomegranate-band-and-space-gray-aluminum-case-pine-green-band-091019_big.jpg.large.jpg", price: 999, stock: 0 },
  { title: "PC Portable - HP  Pavillon 15- 15,6\" FHD", description: "", image_link: "https://www.cdiscount.com/pdt2/2/n/f/1/300x300/hp15eh1022nf/rw/pc-portable-hp-pavillon-15-eh1022nf-15-6-fhd.jpg", price: 499, stock: 12 },
  { title: "Console Nintendo Switch (modèle OLED)", description: "", image_link: "https://www.cdiscount.com/pdt2/4/3/5/1/300x300/45496453435/rw/console-nintendo-switch-modele-oled-nouvelle-v.jpg", price: 339.95, stock: 487 },
  { title: "Machine expresso automatique avec broyeur - DELONGHI", description: "", image_link: "https://www.cdiscount.com/pdt2/3/2/6/1/300x300/del8004399325326/rw/machine-expresso-automatique-avec-broyeur-delong.jpg", price: 319.99, stock: 0 },
  { title: "Salon de jardin en imitation rotin tressé Allibert - 4 personnes", description: "", image_link: "https://www.cdiscount.com/pdt2/t/g/r/1/300x300/sanremo4seatgr/rw/salon-de-jardin-en-imitation-rotin-tresse-allibert.jpg", price: 149.50, stock: 876 },
  { title: "Kickstarter Dirtbike NXD M14 14/12 Moto de cross 125 cc", description: "", image_link: "https://m.media-amazon.com/images/I/71fx0e4a+fL._AC_SX679_.jpg", price: 750, stock: 120 },
]

Meteor.startup(() => {
  if (!Accounts.findUserByEmail(SEED_ADMIN.email)) {
    Accounts.createUser(SEED_ADMIN);
  }
  if (!Accounts.findUserByEmail(SEED_CUSTOMER.email)) {
    Accounts.createUser(SEED_CUSTOMER);
  }
  if (Products.find().count() === 0) {
    data.forEach(e => Products.insert(e));
  }
});
